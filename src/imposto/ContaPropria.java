package imposto;

public class ContaPropria extends Contribuinte {

    private float RT;
    private String profissao;

    private static float txRT = 0.03f;
    private static float txORI = 0.02f;
    private static float txORS = 0.05f;

    public ContaPropria(float RT, String profissao, String nome, String morada, float OR) {
        super(nome, morada, OR);
        this.RT = RT;
        this.profissao = profissao;
    }

    public ContaPropria() {
        super();
        this.RT = 0;
        this.profissao = "sem profissao";
    }

    public float getRT() {
        return RT;
    }

    public void setRT(float RT) {
        this.RT = RT;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public static float getTxRT() {
        return txRT;
    }

    public static void setTxRT(float txRT) {
        ContaPropria.txRT = txRT;
    }

    public static float getTxORI() {
        return txORI;
    }

    public static void setTxORI(float txORI) {
        ContaPropria.txORI = txORI;
    }

    public static float getTxORS() {
        return txORS;
    }

    public static void setTxORS(float txORS) {
        ContaPropria.txORS = txORS;
    }

    @Override
    public float calcImposto() {
        float pagRT = getRT() * txRT;
        if (getOR() > 50000) {
            float pagOR = getOR() * txORS;
            return pagOR + pagRT;
        } else {
            float pagOR = getOR() * txORI;
            return pagOR + pagRT;
        }
    }

}
