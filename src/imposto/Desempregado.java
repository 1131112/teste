package imposto;

public class Desempregado extends Contribuinte{
    
    private int meses;
    
    private float txOR=0.02f;

    public Desempregado(int meses, String nome, String morada, float OR) {
        super(nome, morada, OR);
        this.meses = meses;
    }

    public Desempregado() {
        super();
        this.meses = 0;
    }

    public int getMeses() {
        return meses;
    }

    public void setMeses(int meses) {
        this.meses = meses;
    }

    public float getTxOR() {
        return txOR;
    }

    public void setTxOR(float txOR) {
        this.txOR = txOR;
    }

    @Override
    public float calcImposto() {
        return getOR() * txOR;
    }
    
    
}
