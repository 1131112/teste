package imposto;

public class Reformado extends Contribuinte{

    private float RT;
    
    private static float txOR = 0.03f;
    private static float txRT = 0.01f;

    public Reformado(float RT, String nome, String morada, float OR) {
        super(nome, morada, OR);
        this.RT = RT;
    }

    public Reformado() {
        super();
        this.RT = 0;
    }

    public float getRT() {
        return RT;
    }

    public void setRT(float RT) {
        this.RT = RT;
    }

    public float getTxOR() {
        return txOR;
    }

    public void setTxOR(float txOR) {
        this.txOR = txOR;
    }

    public float getTxRT() {
        return txRT;
    }

    public void setTxRT(float txRT) {
        this.txRT = txRT;
    }
    
    @Override
    public float calcImposto() {
        float pagOR = getOR() * txOR;
        float pagRT = getRT() * txRT;
        return pagOR + pagRT;
    }
    
}
