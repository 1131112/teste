package imposto;

public abstract class Contribuinte {

    private String nome;
    private String morada;
    private float OR;

    public Contribuinte(String nome, String morada, float OR) {
        this.nome = nome;
        this.morada = morada;
        this.OR = OR;
    }

    public Contribuinte() {
        this.nome = "sem nome";
        this.morada = "sem morada";
        this.OR = 0;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public float getOR() {
        return OR;
    }

    public void setOR(float OR) {
        this.OR = OR;
    }

    
    public abstract float calcImposto();
}
