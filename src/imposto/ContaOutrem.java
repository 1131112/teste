package imposto;

public class ContaOutrem extends Contribuinte {

    private float RT;
    private String empresa;
    
    private static float txOR = 0.02f;
    private static float txRTI = 0.01f;
    private static float txRTS = 0.02f;

    public ContaOutrem(float RT, String nome, String morada, float OR, String empresa) {
        super(nome, morada, OR);
        this.RT = RT;
        this.empresa= empresa;
    }

    public ContaOutrem() {
        super();
        this.RT = 0;
        this.empresa = "sem empresa";
    }

    public float getRT() {
        return RT;
    }

    public void setRT(float RT) {
        this.RT = RT;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public static float getTxOR() {
        return txOR;
    }

    public static void setTxOR(float txOR) {
        ContaOutrem.txOR = txOR;
    }

    public static float getTxRTI() {
        return txRTI;
    }

    public static void setTxRTI(float txRTI) {
        ContaOutrem.txRTI = txRTI;
    }

    public static float getTxRTS() {
        return txRTS;
    }

    public static void setTxRTS(float txRTS) {
        ContaOutrem.txRTS = txRTS;
    }

    
    
    @Override
    public float calcImposto() {
        float pagOR = getOR() * txOR;
        if(getRT()>30000){
        float pagRT = getRT() * txRTS;
        return pagOR + pagRT;
        }else{
            float pagRT = getRT() * txRTI;
            return pagOR + pagRT;
        }      
    }

}
